const index = require('../index');

function sphere(number) {
  return (4*Math.PI*number*number*number)/3;
}

function inputSphere() {
  index.rl.question(`Radius (Cm): `, (number) => {
    if (!isNaN(number) && !index.isEmptyOrSpaces(number)) {
      console.log(`\nSphere's Volume: ${sphere(number)} cm³ \n`);
      index.rl.close();
    } else {
      console.log(`Radius must be number`);
      input();
    }
  });
}

module.exports = { inputSphere };
